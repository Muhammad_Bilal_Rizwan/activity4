package com.example.activity4;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private EditText name,surname,email;
    private Button add;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        name = findViewById(R.id.name);
        surname = findViewById(R.id.surname);
        email = findViewById(R.id.email);
        add = findViewById(R.id.ADD);

        add.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String username = name.getText().toString();
                String usersurname = surname.getText().toString();
                String useremail = email.getText().toString();

                Intent intent = new Intent(MainActivity.this,MainActivity2.class);
                intent.putExtra("keyname",username);
                intent.putExtra("keysurname", usersurname);
                intent.putExtra("keyemail", useremail);
                startActivity(intent);


            }
        }));
    }
}
