package com.example.activity4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {

    private TextView name,surname,email;
    private Button back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        name = findViewById(R.id.text_name);
        surname = findViewById(R.id.text_surname);
        email = findViewById(R.id.text_email);

        String username = getIntent().getStringExtra( "keyname");
        String userSurname = getIntent().getStringExtra( "keysurname");
        String userEmail = getIntent().getStringExtra( "keyemail");

        name.setText(username);
        surname.setText(userSurname);
        email.setText(userEmail);

        back = findViewById(R.id.BACK);

        back.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity2.this,MainActivity.class);
                startActivity(intent);

            }
        }));
    }
}